#!/usr/bin/python
# coding=utf-8

__author__= "Maxime Hutinet"

from flask import Flask, request, session, jsonify, send_from_directory
import json
from Simulation import Simulation
from Comment import Comment
from VideoStream import VideoStream
import VideoTools as vt
import Db
import sqlite3
import time
import PDFTools
import os
import Tools as tl
import shutil

app = Flask(__name__)

app.secret_key = 'host'

@app.before_first_request
def init_db():
    Db.create_tables()

"""
SIMULATIONS
"""

@app.route('/simulation', methods=['POST'])
def create_simulation():
    """
    Create a Simulation and run the video streaming/recording
    @return the simulation with all its fields
    """
    try:
        data = json.loads(list(request.form.keys())[0])

        print("0000000000000000000000000000000000000000000000")
        print("Create Simulation")
        print(data)
        print("0000000000000000000000000000000000000000000000")

        current_simulation = Simulation(name=data["name"], cut_duration=data["cutDuration"])

        # Add the video streams of the participants to the simulation
        current_simulation.video_streams = [VideoStream(
            device_name=p["device"]["hostname"], 
            device_ip=p["device"]["ip"], 
            username=p["device"]["username"], 
            password=p["device"]["password"]
            ) for p in data["participants"]]

        # Add the video streams of the main camera to the simulation
        current_simulation.video_streams.append(VideoStream(
            device_name="Main",
            device_ip="0.0.0.0",
            incoming_video_url=data["mainVideoURL"]
        ))

    except KeyError:
        return "Couldn't create simulation, field missing", 400

    print("Start recording")

    current_simulation.start_recording()

    # Insert the Simulation in the database
    current_simulation.id = Db.put_simulation(current_simulation)
    
    # Insert the Video Streams of the Simulation in the database
    for vs in current_simulation.video_streams:
        vs.simulation_id = current_simulation.id
        vs.id = Db.put_video_stream(vs)

    return current_simulation.to_json(), 200

@app.route('/simulation/<id>/save', methods=['POST'])
def save_simulation(id):
    """
    Stop the recording/streaming of the different video streams
    @param id: Primary key of the simulation
    @return the simulation with all its fields
    """
    s = Db.get_simulation(id)

    if s is None:
        return "Couldn't find simulation ", 400

    s.stop_recording()

    return s.to_json()

@app.route('/simulation/<id>', methods=['DELETE'])
def delete_simulation(id):
    """
    Delete a simulation based on it primary key
    @param id: Primary key of the simulation
    """
    s = Db.get_simulation(id)

    # If the simulation was found in the database, we delete along with
    # its comments and video streams
    if s:
        if Db.delete_simulation(id):
            for c in s.comments:
                Db.delete_comment(c.id)
            for vs in s.video_streams:
                Db.delete_video_stream(vs.id)
            
            # We remove the folder containing all the medias
            try:
                shutil.rmtree(s.recording_folder)
            except FileNotFoundError:
                print("Couldn't find " + s.recording_folder)
                pass
        
            return "Simulation " + id + " successfully deleted", 200
    else:
        return "Couldn't delete simulation, simulation id doesn't exist", 400

"""
COMMENTS
"""

@app.route('/simulation/<id>/comment', methods=['POST'])
def add_comment(id):
    """
    Add a comment to a simulation
    @param id: Primary key of the simulation
    @return the newly created comment if it worked
    """
    s = Db.get_simulation(id)
    if s:
        try:
            data = json.loads(list(request.form.keys())[0])

            time_in_simulation = data["timeInSimulation"]
            print(time_in_simulation)
            content = data["content"]
            stream_id = data["videostreamID"]
            print(data)

            c = Comment(
                simulation_id=id,
                content=content,
                time_in_simulation=time_in_simulation
            )

            print("debug1")

            vs = Db.get_video_stream(stream_id)

            print("debug2")

            if vs is None:
                return "Couldn't create com6ment, wrong stream id", 400

            print("debug3")
            
            video_source = s.recording_folder + "/" + vs.filename

            print("debug4")

            filename = time_in_simulation.replace(":", "")

            print("debug5")

            # Extract a thumbnail from the video stream associated to the comment
            c.thumbnail = vt.extract_thumbnail(
                video=video_source,
                time=tl.substract_time(time_in_simulation, 20),
                output_file=s.recording_folder + "/" + filename + ".jpg"
            )

            print("debug6")
            
            c.video_stream_id = stream_id

            print("added comment")
            # Add the comment in the database
            c.id = Db.put_comment(c)
            print(c.id)
            

        except KeyError:
            return "Couldn't create comment, field missing", 400
    else:
        return "Couldn't create comment, simulation id doesn't exist", 400
    return c.to_json()

@app.route('/comment/<id>', methods=['POST'])
def update_comment(id):
    """
    Update the content of a comment in the database
    @param id: Primary key of the comment
    @return the comment modified if the update worked
    """
    c = Db.get_comment(id)
    data = json.loads(list(request.form.keys())[0])
    if c:
        try:
            if Db.update_comment_content(id, data["content"]):
                return Db.get_comment(id).to_json(), 200
            else:
                return "Couldn't update comment", 400
        except KeyError:
            return "Couldn't update comment, field missing", 400
    else:
        return "Couldn't update comment, comment id doesn't exist", 400

@app.route('/comment/<id>', methods=['DELETE'])
def delete_comment(id):
    """
    Delete a comment based on its primary key
    """
    # Retrieve the comment from the database to make sure it exists
    c = Db.get_comment(id)
    if c:
        if Db.delete_comment(id):
            try:
                os.remove(c.thumbnail)
            except FileNotFoundError:
                pass
            return "Successfully deleted comment " + id, 200
        else:
            return "Couldn't delete comment", 400
    else:
        return "Couldn't delete comment, comment id doesn't exist", 400
    
@app.route('/simulation/<path:path>')
def get_file(path):
    """
    Return files present in the simulations folder
    """
    return send_from_directory('static/simulations/', path)

@app.route('/simulation/<id>/debrief/video', methods=['GET'])
def generate_video_debriefing(id):
    """
    Generate a video debriefing based on the primary key of the simulation
    @param id: Primary key of the simulation
    @return the URL where the video debriefing is accessible if it worked
    """
    s = Db.get_simulation(id)

    # If the simulation exists, we get the video stream recording associated to each comment, 
    # cut the videos and add the content of the comments on them
    if s:
        for c in s.comments:
            vs = Db.get_video_stream(c.video_stream_id)
            if vs:
                video_source = s.recording_folder + "/" + vs.filename
                c.video = vt.cut_video(
                        video=video_source.replace("./", ""),
                        time_in_simulation=tl.substract_time(c.time_in_simulation, 20),
                        cut_duration=s.cut_duration,
                        output_file=s.recording_folder.replace("./", "") + "/" + c.content.replace(" ", "_") + "_.ts",
                        text=c.content
                )

        output_file = s.recording_folder + "/" + s.name + ".ts"
        videos = [c.video for c in s.comments if c.video]

        # C oncatenate all the videos to create a montage. Once performed, 
        # remove all the cuts
        if vt.concatenate_videos(videos, output_file):
            for v in videos:
                os.remove(v)
            return {
                "video_url": tl.path_to_url(output_file)
            }, 200
        else:
            "Failed to create simulation", 500

    else:
        return "Couldn't create video, simulation id doesn't exist", 400
    

@app.route('/simulation/<id>/debrief/pdf', methods=['GET'])
def generate_pdf_debriefing(id):    
    """
    Generate a PDF debriefing based on the primary key of the simulation
    @param id: Primary key of the simulation
    @return the URL where the PDF debriefing is accessible if it worked
    """
    print("hello boi")
    s = Db.get_simulation(id)
    if s:
        print("salut")
        print(s.comments)
        output_file = s.recording_folder + "/" + s.name + ".pdf"
        PDFTools.build_pdf(
            comments=s.comments,
            output_file=output_file
        )
        if os.path.exists(output_file):
            return {
                "pdf_url": tl.path_to_url(output_file)
            }, 200
        else:
            return "Couldn't create PDF, internal issue", 400

    else:
        return "Couldn't create PDF, simulation id doesn't exist", 400


if __name__ == '__main__':

    base64.b64encode(bytes(username + ":" + password, 'utf-8')).decode("utf-8")
    app.run(debug=True)
    