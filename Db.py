#!/usr/bin/python
# coding=utf-8

__author__= "Maxime Hutinet"

import sqlite3
from sqlite3 import Error
from Simulation import Simulation
from VideoStream import VideoStream
from Comment import Comment

DB_FILE = './database.db'

SIMULATION_TABLE = "simulation"
COMMENT_TABLE = "comment"
VIDEO_STREAM_TABLE = "video_stream"

CREATE_SIMULATION_TABLE = {"name":SIMULATION_TABLE, 
                           "query": f"""CREATE TABLE IF NOT EXISTS {SIMULATION_TABLE} 
                                        (id INTEGER PRIMARY KEY, 
                                        name TEXT,
                                        recording_path TEXT,
                                        cut_duration TEXT)"""
                          }

CREATE_COMMENT_TABLE = {"name":COMMENT_TABLE, 
                        "query": f"""CREATE TABLE IF NOT EXISTS {COMMENT_TABLE} 
                                    (id INTEGER PRIMARY KEY, 
                                    time_in_simulation TEXT,
                                    thumbnail_path TEXT,
                                    content TEXT,
                                    video_stream_id INTEGER,
                                    video_path INTEGER,
                                    simulation_id INTEGER)"""
                        }

CREATE_VIDEO_STREAM_TABLE = {"name":VIDEO_STREAM_TABLE, 
                             "query": f"""CREATE TABLE IF NOT EXISTS {VIDEO_STREAM_TABLE} 
                                            (id INTEGER PRIMARY KEY, 
                                            url TEXT,
                                            incoming_video_url TEXT,
                                            username TEXT,
                                            password TEXT,
                                            filename TEXT,
                                            device_name TEXT,
                                            device_ip TEXT,
                                            pid_ffmpeg INTEGER,
                                            pid_vlc INTEGER,
                                            simulation_id INTEGER)"""
                            }

tables = [
    CREATE_SIMULATION_TABLE,
    CREATE_COMMENT_TABLE,
    CREATE_VIDEO_STREAM_TABLE
]


"""
UTILS DB
"""

def create_connection():
    """
    Initiate the connection to the database
    @return a reference to the database
    """
    connection = None
    try:
        connection = sqlite3.connect(DB_FILE)
        connection.row_factory = sqlite3.Row
        
    except Error as e:
        print(e)

    return connection

def create_table(connection, name, query):
    """
    Create a table in the database
    @param connection: Link to the database
    @param name: Name of the table
    @param query: Query to create the table
    @return True if the creation worked, else False
    """
    try:
        c = connection.cursor()
        c.execute(query)
        print("[DB] - Creating table " + name)
        return True
    except Error as e:
        print(e)
        return False

def create_tables():
    """
    Create all the necessary tables in the database
    """
    connection = create_connection()
    for q in tables:
        create_table(connection, q["name"], q["query"])

def get_data_by_id(table, id):
    """
    Get an entry from the database based on its table and primary key
    @param table: Table where the entry is
    @param id: primary key of the entry in the table
    @return the data found
    """
    print(id)
    query = f"""SELECT * FROM {table} WHERE id='{id}'"""
    print(query)
    db = create_connection()
    c = db.cursor()
    try:
        c.execute(query)
        r = c.fetchone()
        print("result : " + str(r))
        return r

    except Error as e:
        print("[DB] - Couldn't get id " + id + " from " + table)
        print(e)

def delete_data_by_id(table, id):
    """
    Delete an entry from the database based on its table and primary key
    @param table: Table where the entry is
    @param id: primary key of the entry in the table
    @return True if the deletion worked, else False
    """
    query = f"""DELETE FROM {table} WHERE id='{id}'"""
    db = create_connection()
    try:
        db.execute(query)
        db.commit()
        db.close()
        return True
    except Error as e:
        print("[DB] - Couldn't delete id " + id + " from " + table)
        print(e)
        return False

def get_next_primary_key(table):
    """
    Look for the next available primary key in a table
    @param table: Table to look into
    @return the next primary key available
    """
    query = f"SELECT * FROM {table} ORDER BY id DESC LIMIT 1"
    db = create_connection()
    c = db.cursor()
    try:
        c.execute(query)
        rows = c.fetchone()
        if rows is None:
            return 0
        else:
            return rows["id"] + 1
        
    except Error as e:
        print("[DB] - Couldn't get primary key")
        print(e)

"""
SIMULATIONS
"""

def put_simulation(s):
    """
    Insert a Simulation object into the database
    @param s: Simulation to insert in the database
    @return the primary key of the entry if success
    """
    id = get_next_primary_key(SIMULATION_TABLE)
    query = f"""INSERT INTO {SIMULATION_TABLE} 
            ( id, 
            name,
            recording_path,
            cut_duration ) 
            VALUES ( '{id}', 
            '{s.name}',
            '{s.recording_folder}',
            '{s.cut_duration}' )
            """
    db = create_connection()
    try:
        db.execute(query)
        db.commit()
        db.close()
        return id
    except Error as e:
        print("[DB] - Couldn't insert simulation")
        print(e)

def get_simulation(id):
    """
    Get a simulation from the database based on its primary key
    @param id: Primary key of the simulation in the database
    @return a Simulation object if found
    """
    query = f"""SELECT * FROM {SIMULATION_TABLE} WHERE id='{id}'"""
    db = create_connection()
    c = db.cursor()
    try:
        c.execute(query)
        r = c.fetchone()
        if r is not None:
            s = Simulation(r["name"], r["cut_duration"])
            s.id = id
            s.recording_folder = r["recording_path"]
            s.video_streams = get_all_video_stream_simulation(id)
            s.comments = get_all_comment_simulation(id)
            return s

    except Error as e:
        print("[DB] - Couldn't get primary key")
        print(e)

def delete_simulation(id):
    return delete_data_by_id(SIMULATION_TABLE, id)
    
"""
COMMENTS
"""

def put_comment(c):
    """
    Insert a Comment object into the database
    @param c: Comment to insert in the database
    @return the primary key of the entry if success
    """
    id = get_next_primary_key(COMMENT_TABLE)
    query = f"""INSERT INTO {COMMENT_TABLE} 
            ( id, 
            time_in_simulation, 
            content, 
            video_stream_id, 
            video_path,
            thumbnail_path,
            simulation_id ) 
            VALUES ( '{id}', 
            '{c.time_in_simulation}', 
            '{c.content}', 
            '{c.video_stream_id}', 
            '{c.video}', 
            '{c.thumbnail}', 
            '{c.simulation_id}' )
            """
    db = create_connection()
    try:
        db.execute(query)
        db.commit()
        db.close()
        return id
    except Error as e:
        print("[DB] - Couldn't insert comment")
        print(e)

def get_comment(id):
    """
    Get a comment from the database based on its primary key
    @param id: Primary key of the comment in the database
    @return a Comment object if found
    """
    query = f"""SELECT * FROM {COMMENT_TABLE} WHERE id='{id}'"""
    db = create_connection()
    c = db.cursor()
    try:
        c.execute(query)
        r = c.fetchone()
        com = Comment(
            simulation_id=r["simulation_id"],
            content=r["content"],
            time_in_simulation=r["time_in_simulation"],
        )
        com.id = r["id"]
        com.thumbnail = r["thumbnail_path"]
        com.video = r["video_path"]
        com.video_stream_id = r["video_stream_id"]
        return com

    except Error as e:
        print("[DB] - Couldn't get comment " + id)
        print(e)

def get_all_comment_simulation(simulation_id):
    """
    Retrieve all the comment of a simulation based on its primary key
    @param simulation_id: Primary key of the simulation
    @return A list of Comment object if found, else an empty list
    """
    query = f"""SELECT * FROM {COMMENT_TABLE} WHERE simulation_id='{simulation_id}'"""
    db = create_connection()
    c = db.cursor()
    try:
        c.execute(query)
        rows = c.fetchall()
        res = []
        for r in rows:
            s = Comment(
                simulation_id=simulation_id,
                content=r["content"],
                time_in_simulation=r["time_in_simulation"]
                )
            s.id = r["id"]
            s.video = r["video_path"]
            s.thumbnail = r["thumbnail_path"]
            s.video_stream_id = r["video_stream_id"]
            res.append(s)
        
        return res

    except Error as e:
        print("[DB] - Couldn't get primary key")
        print(e)
        return []

def update_comment_content(id, content):
    """
    Update the content of a comment based on its primary key
    @param id: Primary key of the comment
    @param content: Content of the comment
    @return True if the update worked, else False
    """
    query = f"""UPDATE {COMMENT_TABLE} SET content='{content}' WHERE id='{id}'"""
    db = create_connection()
    try:
        db.execute(query)
        db.commit()
        db.close()
        return True
    except Error as e:
        print("[DB] - Couldn't update comment content")
        print(e)
        return False

def delete_comment(id):
    """
    Delete a comment based on its primary key
    @param id: Primary key of the comment in the database
    @return True if the deletion worked, else False
    """
    return delete_data_by_id(COMMENT_TABLE, id)

"""
VIDEO STREAM
"""

def put_video_stream(vs):
    """
    Insert a Video Stream object into the database
    @param vs: Video stream to insert in the database
    @return the primary key of the entry if success
    """
    id = get_next_primary_key(VIDEO_STREAM_TABLE)
    query = f"""INSERT INTO {VIDEO_STREAM_TABLE} 
            ( id, 
            url,
            incoming_video_url,
            username,
            password, 
            filename, 
            device_name,
            device_ip, 
            pid_ffmpeg,
            pid_vlc,
            simulation_id ) 
            VALUES ( '{id}', 
            '{vs.url}', 
            '{vs.incoming_video_url}', 
            '{vs.username}', 
            '{vs.password}', 
            '{vs.filename}', 
            '{vs.device_name}', 
            '{vs.device_ip}', 
            '{vs.pid_ffmpeg}', 
            '{vs.pid_vlc}', 
            '{vs.simulation_id}' )
            """

    db = create_connection()
    try:
        db.execute(query)
        db.commit()
        db.close()
        return id
    except Error as e:
        print("[DB] - Couldn't insert video stream")
        print(e)

def get_video_stream(id):
    """
    Get a video stream from the database based on its primary key
    @param id: Primary key of the comment in the database
    @return a Video Stream object if found
    """
    r = get_data_by_id(VIDEO_STREAM_TABLE, id)

    print("salut1 : " + str(r))

    if r is None:
        return 

    print("salut2")

    vs = VideoStream(
        device_name=r["device_name"],
        device_ip=r["device_ip"],
        username=r["username"],
        password=r["password"],
        filename=r["filename"],
        url=r["url"],
        incoming_video_url=r["incoming_video_url"],
        pid_ffmpeg=r["pid_ffmpeg"],
        pid_vlc=r["pid_vlc"],
        simulation_id=r["simulation_id"]
    )
    return vs

def get_all_video_stream_simulation(simulation_id):
    """
    Retrieve all the video stream of a simulation based on its primary key
    @param simulation_id: Primary key of the simulation
    @return A list of Video Stream object if found, else an empty list
    """
    query = f"""SELECT * FROM {VIDEO_STREAM_TABLE} WHERE simulation_id='{simulation_id}'"""
    db = create_connection()
    c = db.cursor()
    try:
        c.execute(query)
        rows = c.fetchall()
        res = []
        for r in rows:
            vs = VideoStream(
                device_name=r["device_name"],
                device_ip=r["device_ip"],
                username=r["username"],
                password=r["password"],
                filename=r["filename"],
                url=r["url"],
                incoming_video_url=r["incoming_video_url"],
                pid_ffmpeg=r["pid_ffmpeg"],
                pid_vlc=r["pid_vlc"],
                simulation_id=r["simulation_id"]
            )
            vs.id = r["id"]
            res.append(vs)

        return res

    except Error as e:
        print("[DB] - Couldn't get video streams")
        print(e)
        return []

def delete_video_stream(id):
    """
    Delete a video stream entry based on its primary key
    @param id: Primary key of the video stream in the database
    @return True if the deletion worked, else False
    """
    return delete_data_by_id(VIDEO_STREAM_TABLE, id)









    