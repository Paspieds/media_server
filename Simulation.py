#!/usr/bin/python
# coding=utf-8

__author__= "Maxime Hutinet"

import subprocess
import base64
import time
import os
import Tools as t

class Simulation:
    """
    Class representing a Simulation
    """
    def __init__(self, name, cut_duration):
        self.id = None
        self.name = name
        self.video_streams = []
        self.comments = []
        self.recording_folder = self.create_folder()
        self.cut_duration = cut_duration


    def start_recording(self):
        """
        Run the recording and streaming of the video streams
        """
        port = 8080
        for vs in self.video_streams:
            # If the stream is an URL (Main Video), we first stream it and record the
            # stream we just launched
            if vs.incoming_video_url:
                vs.start_streaming(
                    video_path=None,
                    port=port,
                )
                time.sleep(5)
                vs.record(self.recording_folder)
            
            # Else, we first record and then stream this file
            else:
                print("Recording " + vs.device_name + " with IP " + vs.device_ip + " to " + vs.filename)
                video_path = vs.record(self.recording_folder)
                if video_path:
                    time.sleep(5)
                    vs.start_streaming(video_path, port)
            port += 1

    def stop_recording(self):
        """
        Stop the recording and streaming of each video stream
        """
        for vs in self.video_streams:
            print("Stop recording " + vs.device_name + " with IP " + vs.device_ip)
            vs.stop_recording()

    def create_folder(self):
        """
        Create simulation's folder
        """
        path = '.\static\simulations\{}'.format(self.name)
        if not os.path.exists(path):
            os.mkdir(path)
        return path

    def __str__(self):
        return json.dumps(self.__dict__, indent=2)

    def to_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "video_streams": [vs.to_json() for vs in self.video_streams]
        }
    
