#!/usr/bin/python
# coding=utf-8

__author__= "Maxime Hutinet"

from reportlab.lib.pagesizes import A4
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, Image
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_RIGHT
from reportlab.lib import utils
import os

def add_comment(c):
    """
    Add a comment on the PDF document
    @param c: The comment to add
    @return a list containing the image and a Paragraph objects
    """
    # Add the comment image if it exists, else we had a generic image
    img = add_image(c.thumbnail) if os.path.exists(c.thumbnail) else add_image(EMPTY_IMG)
    return [img, Paragraph(c.time_in_simulation + " : " + c.content, styles["Normal"])]

def add_image(image_path):
    """
    Add an image on the PDF document
    @param image_path: Path of the image in the file system
    @return an Image object
    """
    img = utils.ImageReader(image_path)
    img_width, img_height = img.getSize()
    aspect = img_height / float(img_width)
    return Image(image_path, width=100, height=(100 * aspect))

def add_header():
    """
    Add a header to the PDF document
    """
    return [add_image(LOGO_HUG)]

def build_pdf(comments, output_file):
    """
    Create the PDF document with all the content
    @param comments: A list of comments
    @param output_file: The path of the output file
    """
    doc = SimpleDocTemplate(output_file, pagesize=A4)
    rows=[]
    com = [add_comment(c) for c in comments]
    rows.append(add_image(LOGO_HUG))
    rows.append(Table(com))
    doc.build(rows)


styles = getSampleStyleSheet()
style_right = ParagraphStyle(name='right', parent=styles['Normal'], alignment=TA_RIGHT)
LOGO_HUG = "img/hug.jpg"
EMPTY_IMG = "img/no-img.jpg"
