#!/usr/bin/python
# coding=utf-8

__author__= "Maxime Hutinet"

import json
import subprocess
import base64
import time
import os
import signal
import Tools as t

class VideoStream:
    """
    Class representing a video stream
    """
    def __init__(self, device_name, device_ip=None, incoming_video_url=None, username=None, password=None, filename=None, url=None, pid_ffmpeg=None, pid_vlc=None, simulation_id=None):
        self.id = None
        self.url = url
        self.incoming_video_url = incoming_video_url
        self.username = username
        self.password = password
        self.device_name = device_name
        self.device_ip = device_ip
        self.filename = self.device_name + "_" + self.device_ip + ".ts"
        self.pid_ffmpeg = pid_ffmpeg
        self.pid_vlc = pid_vlc
        self.simulation_id = simulation_id
        print(self.username)

    def record(self, path):
        """
        Record the video stream to a specific file
        @param path: Path where the stream needs to be recorded
        @return the path of the file recorded
        """

        # Variable conditionning the quality of the videos stream we get
        # True for high quality, False for low quality
        HIGH_QUALITY = True

        quality = "high" if HIGH_QUALITY else "low"

        recording_path = path.replace(".\\", "") + "\\" + self.filename
        print("***************recording_path", recording_path)

        print("***************start recording command")
        # If the direct URL was provided then we record directly (Main Camera)
        if self.incoming_video_url:
            print("=============> Saving from Main Camera")
            cmd = 'ffmpeg -y -i "{}" -acodec copy -vcodec copy {}'.format(self.url, recording_path)
        else:
            cmd = 'ffmpeg -y -headers "Authorization: Basic {}" -i "http://{}:{}@{}/api/holographic/stream/live_{}.mp4?holo=true&pv=true&loopback=true" -acodec copy -vcodec copy {}'.format(self.hashCredentials(self.username, self.password), self.username, self.password, self.device_ip, quality, recording_path)
        
        print(cmd)
        p = subprocess.Popen(cmd, shell=False)
        print("***************recording command finish")

        # Save PID if process is running
        if p.returncode is None: 
            self.pid_ffmpeg = p.pid
            return recording_path

        else:
            print("Recording " + self.device_name + " with IP " + self.device_ip + " failed !")
            return None    
        
    def stop_recording(self):
        """
        Stop the recording and streaming processes
        """
        try:
            print("Killing FFMPEG for " + self.device_name)
            t.kill_process(self.pid_ffmpeg)
            print("Killing VLC for " + self.device_name)
            t.kill_process(self.pid_vlc)
        except (ProcessLookupError, ValueError):
            print("Process not running")
        

    def hashCredentials(self, username, password):
        """
        Encode the credentials of the HoloLens glasses with base64
        @param username: Username of the HoloLens
        @param password: Password of the HoloLens
        @return the encoded credentials
        """
        return base64.b64encode(bytes(username + ":" + password, 'utf-8')).decode("utf-8")

    def start_streaming(self, video_path, port):
        """
        Start the streaming. The stream is transcoded if it's from the main camera
        @param video_path: Path of the video to stream
        @param port: Port on which the video will be streamed
        """
        cmd = ""

        if self.incoming_video_url:
            print("Start streaming. incoming_video_url:", self.incoming_video_url)
            cmd = 'vlc ' + self.incoming_video_url + ' :sout=#transcode{vcodec=h264,vb=800,scale=Automatique,acodec=none,scodec=none}:http{mux=ts,dst=:' + str(port) + '/} :no-sout-all :sout-keep' 
        else:
            cmd = 'vlc ' +  video_path + ' --loop :sout=#http{mux=ts,dst=:' + str(port) + '/} :no-sout-all :sout-keep'
        print(cmd)
        p = subprocess.Popen(cmd, shell=False)

        if p.returncode is None:
            print("Start streaming command return is null")
            self.pid_vlc = p.pid
            # if self.incoming_video_url: 
            #     # self.url = self.incoming_video_url
            #     print("Nothing to do")
            # else:
            self.url = "http://" + t.get_ip_address() + ":" + str(port)
            print("self.url", self.url)


    def __str__(self):
        return json.dumps(self.__dict__, indent=2)

    def to_json(self):
        return {
            "device_ip": self.device_ip,
            "device_name": self.device_name,
            "incoming_video_url": self.incoming_video_url,
            "id": self.id,
            "url": self.url,
        }


