#!/usr/bin/python
# coding=utf-8

__author__= "Maxime Hutinet"

import socket
import subprocess
import datetime
from datetime import timedelta

def get_ip_address():
    """
    Get the IP address of the server
    @return IP address of the server
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]

def path_to_url(path):
    """
    Generate a URL from a server path
    @param path: Path to convert to URL
    @return The URL corresponding to the path
    """
    if path is None:
        return ""
    p = ""

    # We have to perform a different operation depending of the beginning of the path
    if path.startswith('.\\'):
        p = path.replace('.\\static\simulations', 'http://' + get_ip_address() + ":5000/simulation")
    else:
        p = path.replace('static\simulations', 'http://' + get_ip_address() + ":5000/simulation")
    return p

def kill_process(pid):
    """
    Kill a process based on its PID (Process Identifier)
    """
    if pid is None:
        return
    cmd = "taskkill /F /PID " + str(pid)
    subprocess.Popen(cmd, shell=False)

def substract_time(time, duration):
    """
    Substract a duration from a time
    @param time: Initial time to reduce
    @param duration: Duration du deduct in second
    @return Modified time
    """
    ts = datetime.datetime.strptime(time, '%H:%M:%S')
    return str((ts - timedelta(seconds=int(duration)))).split(" ")[1]