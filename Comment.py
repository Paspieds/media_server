#!/usr/bin/python
# coding=utf-8

__author__= "Maxime Hutinet"

import Tools as t
import json

class Comment:
    """
    Class representing a comment
    """
    def __init__(self, simulation_id, content, time_in_simulation):
        self.id = None
        self.simulation_id = simulation_id
        self.content = content
        self.time_in_simulation = time_in_simulation
        self.video = None
        self.thumbnail = None
        self.video_stream_id = None

    def __str__(self):
        return json.dumps(self.__dict__, indent=2)

    def to_json(self):
        return {
            "id": self.id,
            "content": self.content,
            "time_in_simulation": self.time_in_simulation,
            "thumbnail": t.path_to_url(self.thumbnail)
        }

