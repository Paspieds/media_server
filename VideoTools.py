#!/usr/bin/python
# coding=utf-8

__author__= "Maxime Hutinet"

import subprocess
import datetime
from datetime import timedelta
import os

def get_start_end_time(duration, time):
    """
    Return the starting and ending time of a video based on its duration 
    and the time at which the comment was taken
    @param duration: Duration of the cut
    @param time: Time where the video starts
    @return a tuple with the start and end time
    """
    ts = datetime.datetime.strptime(time, '%H:%M:%S')
    start = str(ts).split(" ")[1]
    end = str((ts + timedelta(seconds=int(duration)))).split(" ")[1]
    return (start, end)

def cut_video(video, time_in_simulation, cut_duration, output_file, text):
    """
    Cut a video based on the time in the video and the cut duration.
    Once the cut performed, the text is added on the clip.
    @param video: Path of the video
    @param time_in_simulation: Time in the simulation/video
    @param cut_duration: Duration of the final clip
    @param text: Content of the text to add on the video
    @return the path to the final clip
    """
    cutting_time = get_start_end_time(duration=cut_duration, time=time_in_simulation)

    print("[FFMPEG] - Creating video {} with cut from {} to {}".format(video, cutting_time[0], cutting_time[1]))
    cmd = f'ffmpeg -y -i {video} -ss {cutting_time[0]} -to {cutting_time[1]} -c:v copy -c:a copy {output_file}'
    p = subprocess.Popen(cmd, shell=True,universal_newlines=True)

    while(True):
        if p.poll() != None:
            if os.path.exists(output_file):
                return write_on_video(text, output_file)
            else:
                return None

def write_on_video(text, video):
    """
    Write on a video clip
    @param text: Text to add on the video clip
    @param video: Path to the video
    @return path of the final clip
    """
    output = video.replace("_.ts", ".ts")
    cmd = f'ffmpeg -y -i {video} -vf drawtext="fontfile=C\\:/Windows/Fonts/arial.ttf:fontsize=50:fontcolor=black:box=1:boxcolor=white@1.0:x=(w-text_w)/2:y=(h-text_h):text=\'{text}\'" -vcodec libx264 {output}'
    p = subprocess.Popen(cmd, shell=True)

    while(True):
        if p.poll() != None:
            if os.path.exists(output):
                os.remove(video)
                return output
            else:
                return None

def concatenate_videos(videos, output_file):
    """
    Concatenate multiple video clip
    @param videos: A list of video clip path
    @param output_file: Path of the final video clip
    @return the path of the final clip if the concatenation worked, else None
    """
    videos = list(filter(None, videos)) 
    input = "|".join(videos)
    #cmd = f'ffmpeg -y -i "concat:{input}" -codec copy {output_file}'
    cmd = f'ffmpeg -y -i "concat:{input}" -max_muxing_queue_size 9999 -codec copy {output_file}'
    p = subprocess.Popen(cmd, shell=True)

    while(True):
        if p.poll() != None:
            if os.path.exists(output_file):
                return output_file
            else:
                return None

def extract_thumbnail(video, time, output_file):
    """
    Extract a frame from a video
    @param video: Video on which the frame needs to be extracted
    @param time: Time in the video where the frame needs to be taken
    @param output_file: Path of the frame extracted
    @return the path of the frame file if it worked, else None
    """
    cmd = f"ffmpeg -y -i {video} -ss {time} -vframes 1 {output_file}"
    print(cmd)
    p = subprocess.Popen(cmd, shell=True)

    while(True):
        if p.poll() != None:
            if os.path.exists(output_file):
                return output_file
            else:
                return None
