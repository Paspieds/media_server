# Media Server 

This is an implementation of the media server component of Maxime Hutinet's Bachelor Thesis. 

This component provides :

* Recording and streaming capabilities
* Video and PDF generation capabilities

# How to run the app ?

Run the following on Windows

```
.\venv\Scripts\activate
$env:FLASK_APP = "main.py"
$env:FLASK_ENV = "development"
flask run --host=0.0.0.0
```

For the Powershell script, make sure the permission are `RemoteSigned`

```

PS C:\Users\ASUS_hepia_2> Get-ExecutionPolicy
RemoteSigned
```

Run the following on Mac

```
source venv/bin/activate
export FLASK_APP=main
export FLASK_ENV=development
flask run --host=0.0.0.0
```
